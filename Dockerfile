FROM node:16.18.1
ENV HOST 0.0.0.0
ENV PORT 3000
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
COPY app.js /app/src/
COPY auth.js /app/src/
CMD ["node","src/app.js","--host", "0.0.0.0"]
EXPOSE 3000
